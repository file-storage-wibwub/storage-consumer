export interface createMetaDataDTO {
  objectId: string;
  path: string;
  buffer: Buffer;
  contentType: string;
  expiresDate: Date;
  uuid: string;
}

export interface deleteFileDTO {
  objectId: string;
}

export interface updateFileDTO {
  objectId: string;
  contentType: string;
  buffer: Buffer;
}
