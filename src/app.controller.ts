import { Controller, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { MessagePattern, Payload } from '@nestjs/microservices';
import * as fs from 'node:fs/promises';
import {
  createMetaDataDTO,
  deleteFileDTO,
  updateFileDTO,
} from './dto/metadata.dto';
import { PrismaService } from './prisma/prisma.service';

@Controller()
export class AppController {
  private logger: Logger;
  constructor(
    private readonly prismaService: PrismaService,
    private readonly configService: ConfigService,
  ) {
    this.logger = new Logger();
  }

  @MessagePattern('create_metadata')
  async createMetadata(@Payload() payload: createMetaDataDTO) {
    const storagePath = this.configService.get('storage_path', '');
    this.logger.log('consume create file objectId ' + payload.objectId);
    const { buffer, ...logdata } = payload;
    this.logger.debug(logdata);
    await fs.writeFile(storagePath + '/' + payload.path, Buffer.from(buffer));
    this.logger.debug('write file to local storage complete');
    this.logger.log(
      'update objectId ' + payload.objectId + ' to status complete',
    );
    this.logger.debug({
      uploadedAt: new Date(),
      expiresDate: payload.expiresDate,
      objectID: payload.objectId,
      status: 'complete',
      path: payload.path,
      contentType: payload.contentType,
    });
    const data = await this.prismaService.file.update({
      data: {
        uploadedAt: new Date(),
        expiresDate: payload.expiresDate,
        objectID: payload.objectId,
        status: 'complete',
        path: payload.path,
        contentType: payload.contentType,
      },
      where: { uuid: payload.uuid },
    });
    return {
      data: data,
      status: 'ok',
    };
  }

  @MessagePattern('update_file')
  async updateFile(@Payload() payload: updateFileDTO) {
    const storagePath = this.configService.get('storage_path', '');
    this.logger.log('consume update file objectId ' + payload.objectId);
    const data = await this.prismaService.file.findFirst({
      where: { objectID: payload.objectId },
    });

    if (!data) {
      this.logger.error('invalid object id objectId ' + payload.objectId);
      return {
        error: 'invalid object id',
      };
    }

    if (data.status !== 'complete') {
      this.logger.error('file is not index yet objectId ' + payload.objectId);
      return {
        error: 'file is not index yet',
      };
    }
    this.logger.log('replacing file objectId ' + payload.objectId);
    await fs.writeFile(
      storagePath + '/' + data.path,
      Buffer.from(payload.buffer),
    );
    this.logger.debug('write update file to local storage complete');
    this.logger.log('updating.. content type of objectId ' + payload.objectId);
    this.logger.debug(
      'contentType from ' + data.contentType + ' to ' + payload.contentType,
    );
    const update = await this.prismaService.file.update({
      where: { uuid: data.uuid },
      data: {
        contentType: payload.contentType,
      },
    });
    this.logger.debug(
      'update completed content type of objectId ' + payload.objectId,
    );
    return {
      data: update,
      status: 'ok',
    };
  }

  @MessagePattern('delete_file')
  async deleteFile(@Payload() payload: deleteFileDTO) {
    this.logger.log('consume delete file objectId ' + payload.objectId);
    const data = await this.prismaService.file.findFirst({
      where: { objectID: payload.objectId },
    });
    const update = await this.prismaService.file.update({
      where: { uuid: data.uuid },
      data: {
        isDelete: true,
        deletedAt: new Date(),
      },
    });
    this.logger.debug(
      'delete completed content type of objectId ' + payload.objectId,
    );
    return {
      data: update,
      status: 'ok',
    };
  }
}
