import { Logger } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import { AppModule } from './app.module';

async function bootstrap() {
  const kafkaURL = process.env.kafkaURL || 'localhost:9093';
  const app = await NestFactory.createMicroservice<MicroserviceOptions>(
    AppModule,
    {
      transport: Transport.KAFKA,
      options: {
        client: {
          brokers: [kafkaURL],
          retry: {
            maxRetryTime: 3,
          },
        },
        consumer: {
          groupId: 'storage-consumer-group',
        },
      },
    },
  );

  const logger = new Logger('Storage-Consumer');
  logger.log('consumer starting ...');
  await app.listen();
  logger.log('consumer started with broker on ' + kafkaURL);
}
bootstrap();
